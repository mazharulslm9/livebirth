<?php

namespace App\registration\child;

use App\registration\Utility\Utility;

class Child {

    public $id = "";
    public $fname = "";
    public $mname = "";
    public $lname = "";
    public $suffix = "";
    public $tob = "";
    public $sex = "";
    public $dob = "";
    public $fcname = "";
    public $lob = "";
    public $country = "";

    public function __construct($data = false) {

        if (is_array($data) && array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        $this->fname=$data['fname'];
        $this->mname=$data['mname'];
        $this->lname=$data['lname'];
        $this->suffix=$data['suffix'];
        $this->tob=$data['tob'];
        $this->sex=$data['sex'];
        $this->dob=$data['dob'];
        $this->fcname=$data['fcname'];
        $this->lob=$data['lob'];
        $this->country=$data['country'];
  
        
        
    }

    public function index() {
        $child=array();
        $con = mysql_connect("localhost", "root", "") or die("Cannot Connect to database");
        $lnk = mysql_select_db("livebirth") or die("Cannot Select database");

        $query = "SELECT * FROM `child`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $child[] = $row;
        }
        return $child;
    }

    public function show($id = false) {

        $con = mysql_connect("localhost", "root", "") or die("Cannot Connect to database");
        $lnk = mysql_select_db("livebirth") or die("Cannot Select database");

        $query = "SELECT * FROM `child` WHERE id =" . $id;
        $result = mysql_query($query);

        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function store() {
       
        $con = mysql_connect("localhost", "root", "") or die("Cannot Connect to database");
        $lnk = mysql_select_db("livebirth") or die("Cannot Select database");

        $query = "INSERT INTO `livebirth`.`child` (`fname`, `mname`, `lname`, `suffix`, `tob`, `sex`, `dob`, `fcname`, `lob`, `country`) VALUES ('".$this->fname."', '".$this->mname."', '".$this->lname."', '".$this->suffix."', '".$this->tob."', '".$this->sex."', '".$this->dob."', '".$this->fcname."', '".$this->lob."','".$this->country."');";
        $result = mysql_query($query);
        if ($result) {
            Utility::message("child's info successfully inserted");
        } else {
            Utility::message("child's info is not inserted, try again later");
        }
        Utility::redirect('index.php');
    }

    public function update() {

        $con = mysql_connect("localhost", "root", "") or die("Cannot Connect to database");
        $lnk = mysql_select_db("livebirth") or die("Cannot Select database");

        $query = "UPDATE `atomicproject`.`books` SET `title` = '" . $this->title . "' WHERE `books`.`ID` = " . $this->id;
        $result = mysql_query($query);
        if ($result) {
            Utility::message("Book Title is successfully Edited");
        } else {
            Utility::message("Book Title is not inserted, try again later");
        }
        Utility::redirect('index.php');
    }

    public function delete($id = null) {
        if (is_null($id)) {
            Utility::Message("No, id available sorry");
            return Utility::redirect("index.php");
        }

        $con = mysql_connect("localhost", "root", "") or die("Cannot Connect to database");
        $lnk = mysql_select_db("livebirth") or die("Cannot Select database");

        $query = "DELETE FROM `atomicproject`.`books` WHERE `books`.`ID` = " . $id;

        $result = mysql_query($query);
        if ($result) {
            Utility::message("Book Title is successfully Deleted");
        } else {
            Utility::message("Book can not delete");
        }
        Utility::redirect('index.php');
    }

}
